import { Component, OnInit } from '@angular/core';
import { ClassifiedService } from '../classified.service';
import { Observable } from 'rxjs';
//import { ImageService } from '../image.service';
//import { ClassifyService } from '../classify.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  body:String;
  category:String;
  articles$:Observable<any>;
  //categoryImage:string; 


  constructor(private classifiedService: ClassifiedService) { }
  /** 
  constructor( public classifyService:ClassifyService,
                private classifiedService: ClassifiedService
               ,public imageService:ImageService) { }*/

  ngOnInit() {
    this.articles$ = this.classifiedService.getArticles();
/** 
 this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
      }
    )*/
  }

  deleteArticle(id:string){
    this.classifiedService.deleteArticle(id);
  }
}
