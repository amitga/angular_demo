import { Router } from '@angular/router';
import { Users } from './interfaces/users';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<Users | null>;
  
  constructor(public afAuth:AngularFireAuth,
              private router:Router) {
    this.user = this.afAuth.authState;
  }

  getUser(){
    return this.user
  }

  SignUp(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(res => 
          {
            console.log('Succesful Signup', res);
            this.router.navigate(['/welcome']);
          }
      )
        .catch(function(error) {
        // Handle Errors here.
        // var errorCode = error.code;
        var errorCode = error.code;
        var errorMessage = error.message;
        alert(errorMessage);
        console.log(error);
        console.log(errorCode);
        this.router.navigate(['/login']);
   });
  }

  Logout(){
    this.afAuth.auth.signOut()
      .then(
        res =>  
         {
           console.log('Succesful Logout',res);
           this.router.navigate(['/generals']);
          }
      )
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(res =>  
            {
              console.log('Succesful Login',res);
              this.router.navigate(['/welcome']);
            }     
        )
        .catch(function(error) {
          // Handle Errors here.
          // var errorCode = error.code;
          var errorCode = error.code;
          var errorMessage = error.message;
          alert(errorMessage);
          console.log(error);
          console.log(errorCode);
          this.router.navigate(['/login']);
     });
  }



}
