import { TestBed } from '@angular/core/testing';

import { ClassifiedService } from './classified.service';

describe('ClassifiedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClassifiedService = TestBed.get(ClassifiedService);
    expect(service).toBeTruthy();
  });
});
