/**
import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralsService } from './../generals.service';
import { Component, OnInit } from '@angular/core';
 
@Component({
  selector: 'app-generalform',
  templateUrl: './generalform.component.html',
  styleUrls: ['./generalform.component.css']
})

export class GeneralformComponent implements OnInit {

  constructor(private generalsService:GeneralsService,
              private authService:AuthService, 
              private router:Router,
              private route: ActivatedRoute) { }

  title:string;
  author:string; 
  id:string;
  userId:string;
  isEdit:boolean = false;
  buttonText:string = 'Add general' 
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.generalsService.updateGeneral(this.userId, this.id,this.title,this.author);
    } else {
      console.log('In onSubmit');
      this.generalsService.addGeneral(this.userId,this.title,this.author)
    }
    this.router.navigate(['/generals']);  
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
        if(this.id) {
          this.isEdit = true;
          this.buttonText = 'Update general'
          this.generalsService.getGeneral(this.userId,this.id).subscribe(
            general => {
            console.log(general.data().author)
            console.log(general.data().title)
            this.author = general.data().author;
            this.title = general.data().title;})        
      }
   })
  }
}
*/

import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralsService } from './../generals.service';
import { Component, OnInit } from '@angular/core';
 
@Component({
  selector: 'app-generalform',
  templateUrl: './generalform.component.html',
  styleUrls: ['./generalform.component.css']
})

export class GeneralformComponent implements OnInit {

  constructor(private generalsService:GeneralsService,
              private authService:AuthService, 
              private router:Router,
              private route: ActivatedRoute) { }

  title:string;
  room:string; 
  date:Date;
  id:string;
  userId:string;
  isEdit:boolean = false;
  buttonText:string = 'Add general' 
  rooms:object[] = [{id:1, name:'Conference room'},{id:2, name:'Meeting Room'},
                     {id:3, name:'lobby'}, {id:4, name:'102'}];
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.generalsService.updateGeneral(this.userId, this.id,this.title,this.room,this.date);
    } else {
      console.log('In onSubmit');
      this.generalsService.addGeneral(this.userId,this.title,this.room,this.date)
    }
    this.router.navigate(['/generals']);  
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
        if(this.id) {
          this.isEdit = true;
          this.buttonText = 'Update general'
          this.generalsService.getGeneral(this.userId,this.id).subscribe(
            general => {
            console.log(general.data().room)
            console.log(general.data().title)
            console.log(general.data().date)
            this.room = general.data().room;
            this.title = general.data().title;
            this.date = general.data().date;})        
      }
   })
  }
}