export interface Users {
    uid:string,
    email?: string | null,
    photoUrl?: string,
    displayName?:string
    }     
