import { Router, RouterModule } from '@angular/router';
import { Main } from './interfaces/main';
import { Photos } from './interfaces/photos';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  apiUrl='https://jsonplaceholder.typicode.com/posts/';
  apiUrl_photos='https://jsonplaceholder.typicode.com/todos';


  constructor(private http: HttpClient, private db: AngularFirestore,public router:Router) { }

/* GETING FROM JSON*/
getMains(){
  return this.http.get<Main[]>(this.apiUrl)
}

getPhotos(){
  return this.http.get<Photos[]>(this.apiUrl_photos)
}

addTodo(title:string, completed:boolean){
  const todo = {title:title, completed:completed}
  this.db.collection('todos').add(todo)  
  this.router.navigate(['/todos']);
}



addArticle(category:String, body:String, img:string){
  const article = {category:category, body:body, img:img}
  this.db.collection('articles').add(article)  
  this.router.navigate(['/articles']);
}


}
