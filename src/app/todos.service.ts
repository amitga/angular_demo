import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private db: AngularFirestore) { }

  getTodos():Observable<any[]>{
    return this.db.collection('todos').valueChanges({idField:'id'});
  }
}
